OUTPUT_DIR = ./bin
OUTPUT_BIN = ${OUTPUT_DIR}/PROG
OUTPUT = -o ${OUTPUT_BIN}
SOURCES = ./cmd/lock.c

build: output_dir
	gcc -Wall ${SOURCES} ${OUTPUT:PROG=package-lock}

output_dir:
	mkdir -p ${OUTPUT_DIR}

install:
	mv ${OUTPUT_BIN:PROG=package-lock} /usr/sbin/
