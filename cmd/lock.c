#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <getopt.h>
#include <unistd.h>

int main(int argc, char **argv) {
	struct flock fl;
	int c;
	int sleep_time;
	int fd;

	static const struct option long_options[] = {
		{"file", required_argument, NULL, 'f'},
		{"sleep", required_argument, NULL, 's'},
		{0, 0, 0, 0}
	};

	while(1) {
		int option_index = 0;
		c = getopt_long(argc, argv, "f:s:", long_options, &option_index);

		if (c == -1) {
			break;
		}

		// No input sanitation
		switch(c) {
			case 'f':
				fd=open(optarg, O_RDONLY);
				break;
			case 's':
				sleep_time = atoi(optarg);
				break;
		}
	}

	fl.l_type=F_RDLCK;
	fl.l_whence=SEEK_SET;
	fl.l_start=0;
	fl.l_len=0;
	if(fcntl(fd,F_SETLK,&fl) == -1) {
		printf("ERROR\n");
		return 1;
	}
	do {
		sleep(sleep_time);
		break;
	} while(1);

	return 0;
}
