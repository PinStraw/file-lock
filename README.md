# File Lock Program

This is a simple file lock program which takes two arguments.

- -f: File which will be locked
- -s: Number of seconds to lock the file

## Install

```
make build
sudo make install
```

